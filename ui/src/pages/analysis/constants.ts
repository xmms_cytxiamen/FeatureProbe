export const CUSTOM = 'CUSTOM';
export const PAGE_VIEW = 'PAGE_VIEW';
export const CLICK = 'CLICK';

export const CONVERSION = 'CONVERSION';
export const COUNT = 'COUNT';
export const DURATION = 'DURATION';
export const REVENUE = 'REVENUE';

export const NEGATIVE = 'NEGATIVE';
export const POSITIVE = 'POSITIVE';

export const SIMPLE = 'SIMPLE';
export const EXACT = 'EXACT';
export const SUBSTRING = 'SUBSTRING';
export const REGULAR = 'REGULAR';
